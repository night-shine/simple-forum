import React from 'react';
import { Headers } from './components/header/Headers';
import { DisplayChat } from "./components/display-chat/DisplayChat"

import './App.css';

function App() {
  return (
    <div className="App">
      <header>
        <Headers />
      </header>
      <section>
        <DisplayChat />
      </section>
    </div>
  );
}

export default App;
