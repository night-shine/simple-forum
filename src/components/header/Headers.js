
import "./header-style.css"

export const Headers = () => {
    return (
        <div>
            <div className="header-box">
                <div className="header-chid">
                    <button className="header-item">Topics</button>
                    <button className="header-item">Top</button>
                </div>
                <div className="header-child">
                    <input id="search" type="text" placeholder="Search" />
                    <button>Search</button>
                </div>
            </div>
        </div>
    )
}