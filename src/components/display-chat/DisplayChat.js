import React from "react";

import "./display-chat-style.css"
import User from "./icons/User"

export const DisplayChat = () => {
    return (
        <div className="chat-section">
            <div className="chat-item">
                <div className="chat-box">
                    <User />
                    <h1>Title</h1>
                </div>
                <div className="chat-box">
                    <User />
                    <h1>Title</h1>
                </div>
                <div className="chat-box">
                    <User />
                    <h1>Title</h1>
                </div>
            </div>
        </div>
    )
}